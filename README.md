# vim-text-req
---

### Introduction
A Vim filetype plugin used to write requirement in text format.

### Feature
+ **Vim-Script, RegExp**
+ Support to number parameter before the command.
+ Syntax and keyword highlight, flodding, abbreviate.
    
